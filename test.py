#!/usr/bin/python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import QTimer
from main import *
import platform
import uuid
import MySQLdb as mdb
import sys
import time
import os.path
import os
import commands
import urllib2
import urllib

# globals

global osname
global path
global uid
global ufile
global username
global server
global puser 

# mysql generated puserid (leave as none)
puser = None

## server address + path (change this line)

server = '127.0.0.1'

# get the O.S

osname = platform.system()

# set temporary file path based on O.S

if osname == 'Linux':
    path = '/var/tmp/uid.txt'
if osname == 'Windows':
    path = 'C:\Windows\Temp\uid.txt'

# Update console Window


def updateconsole(text):
    consolewindow.append(text)


# ufile path

ufile = os.path.exists(path)

# restart program


def restart_program():
    python = sys.executable
    os.execl(python, python, *sys.argv)


# User-name Dialogue Box


def chooseusername():

    # choose a user-name

    global username
    (username, ok) = QInputDialog.getText(myapp, 'username',
            'choose a username:')
    consolewindow.append('Username: ' + username)

    # process account info

    username = str(username)

    # set the username label

    welcome.setText(username)
    
    # direct connection to mysql added in Test-0.7
    try:
	# make connection 
	con = mdb.connect(server, 'dev', '1234', 'pybbs');
	cur = con.cursor()
	cur.execute("INSERT INTO puser VALUES (%s,%s,%s,%s) ", (puser,uid, username,cpname))
	con.commit()
    except mdb.Error, e:
	# write log for debugging
	error = "Error %d: %s" % (e.args[0],e.args[1])
	f = open('dblog.txt', 'w')
	f.write(str(error))
	f.close()	
	sys.exit(1)
     # let user know their account has been created 
    QtCore.QTimer.singleShot(4000, lambda : \
                                 updateconsole('Account created successfully'
                                 ))
    # login notification
    QtCore.QTimer.singleShot(4000, lambda : \
	                             updateconsole('Loggin you in...'
	                             ))    
     # now login        
    QTimer.singleShot(4000, loginuser)


# Generate uid

def generateuid():
    QtCore.QTimer.singleShot(1000, lambda : \
                             updateconsole('Searching for account'))
    QtCore.QTimer.singleShot(4000, lambda : \
                             updateconsole('Generating new account, please wait'
                             ))
    global uid
    global cpname
    uid = uuid.uuid1()
    f = open('uid.txt', 'w')
    f.write(str(uid))
    f.close()

        # run commands based on O.S version

    if osname == 'Linux':
	# Updated in Test0.7-2 removed sudo 
        commands.getoutput('mv uid.txt  /var/tmp/uid.txt')
	# Updated in Test0.7-2 
        #commands.getoutput('rm uid.txt')
    if osname == 'Windows':
	# changed in test-0.6 to fix issue #4
        os.system('move uid.txt  C:\Windows\Temp\uid.txt')
       
    # get the computer name for Identification

    cpname = platform.node()

    # choose username dialogue box

    username = QTimer.singleShot(4000, chooseusername)


# loginuser


def loginuser():

        # update console Info

    QtCore.QTimer.singleShot(1, lambda : \
                             updateconsole('Searching for Account'))
    QtCore.QTimer.singleShot(6000, lambda : \
                             updateconsole('Verifying account info'))

    # username global

    global uname

        # open uid file

    f = open(path, 'r')

    # read uid from file

    uid = f.read()

    # get computer name

    cpname = platform.node()

    # update console info

    QtCore.QTimer.singleShot(4000, lambda : \
                             updateconsole('Signing in, please wait'))


# check if the userid file exist


def checkuid(ufile):
    if ufile == False:
        generateuid()
    if ufile == True:
        loginuser()


class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):

        # some more globals

        global consolewindow
        global timer
        global username
        global cpname
        global welcome

        # build parent user interface

        super(MainWindow, self).__init__()
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # console window

        consolewindow = self.ui.console_rb
        welcome = self.ui.welcome_lbl


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    myapp = MainWindow()
    myapp.show()
    try:
        checkuid(ufile)
    except KeyboardInterrupt:
        print 'Interrupted'
        sys.exit(0)
    sys.exit(app.exec_())
